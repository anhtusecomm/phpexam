<?php
error_reporting(0);
class Bai3{
    var $x;

    function run(){
        //Nhap x gan gia tri $x
        echo "Moi ban nhap vao gia tri cua bien so x: ";
        $handle = fopen ("php://stdin","r");
        $input=fgets($handle);
        $this->x = rtrim("$input");
    }
    function getGiaiThua(){

        $factorial = 1;
        for ($i=5; $i>=1; $i--)
        {
            $factorial = $factorial * $i;
        }
        return $factorial;
    }
    function getKetQua(){
        echo "\nGia tri cua ham so f($this->x) = "
            .round( ($this->x) +
                ((pow($this->x, 5)) / ($this->getGiaiThua())) +
                    (sqrt(abs($this->x)) / (pow($this->x, 3/2))),2);
    }
}

$shell = new Bai3();
$shell->run();
echo $shell->getKetQua();
?>
