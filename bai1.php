<?php
error_reporting(0);
class Bai1{
    var $a , $b ;

    function run(){
        //Nhap a gan gia tri $a
        echo "Nhap ban nhap so a:";
        $handle = fopen ("php://stdin","r");
        $input=fgets($handle);
        $this->a = rtrim("$input");

        //Nhap b gan gia tri $b
        echo "Nhap ban nhap so b:";
        $input=fgets($handle);
        $this->b = rtrim("$input");
        fclose($handle);
    }

    function getKetQua(){
        echo "\nTong: " .$this->a ." + " .$this->b."= ".($this->a + $this->b);
        echo "\nHieu: ".$this->a ." - " .$this->b."= ".($this->a - $this->b);
        echo "\nTich: ".$this->a ." * " .$this->b."= ".($this->a * $this->b);
        echo "\nThuong: ".$this->a ." / " .$this->b."= ".floor($this->a / $this->b);
        echo "\nChia Lay Du: ".$this->a ." % " .$this->b."=".($this->a % $this->b);
        echo "\nChia Le (Lam tron 2 so le): ".$this->a ." / " .$this->b."= ".round(($this->a / $this->b),2);
    }
}

$shell = new Bai1();
$shell->run();
$shell->getKetQua();

?>
